
function Couteau(x,y,vitesseCouteau)
{
	Obstacle.call(this,x,y,10,10,"red");

	this.posBase = x;
	this.vitesse = vitesseCouteau;
  
  // load the spritesheet
  this.spritesheet = libImg.getImage("feu");
  this.spritesheet.width = 155;  // 930 / 6
  this.spritesheet.height = 150;  

  this.offset = 0;
  this.currentFrame = 0;
  this.then = performance.now();
  this.totalTimeSinceLastRedraw = 0;
  this.delayBetweenFrames = 1000 / 20;  // delay in ms between images,  this.setNbImagesPerSecond(20);


  this.draw = function(env) {
    // Use time based animation to draw only a few images per second
    var now = performance.now();
    var delta = now - this.then;  
    

    //*\\//*\\//*\\ DRAW //*\\//*\\//*\\ 
    ctxCanvas = env.getContextCanvas();
    ctxCanvas.save();
    ctxCanvas.beginPath();
    ctxCanvas.translate(this.posx, this.posy + env.posyA);

    var scale = 4;
    ctxCanvas.drawImage(this.spritesheet, this.offset, this.height * scale, this.spritesheet.width, this.spritesheet.height, -15, -5, this.width * scale, this.height * scale);
    if (DEBUG) {ctxCanvas.rect(0, 0, this.width,this.height);}

    ctxCanvas.fillStyle = this.color;
    ctxCanvas.fill();  
    ctxCanvas.closePath();
    ctxCanvas.restore();
    //*\\//*\\//*\\ DRAW //*\\//*\\//*\\

    if (this.totalTimeSinceLastRedraw > this.delayBetweenFrames) {
      
       // Go to the next sprite image
      this.currentFrame++; 
      this.currentFrame %=  6;  //this.spriteArray.length;  == nbImage
      this.offset += this.spritesheet.width;
      
      if(this.currentFrame % 6 == 0) {
         this.offset = 0;
       }
      //console.log("Offset " + this.offset);  // valeur max offset = 775
          
      // reset the total time since last image has been drawn
      this.totalTimeSinceLastRedraw = 0;
    } 

    else {
      // sum the total time since last redraw
      this.totalTimeSinceLastRedraw += delta;
    }

    this.then = now;
  };
  

 
    this.move = function(env)
  {
  	if (this.vitesse == 0){return;}
    var widthCanvas = env.getCanvasWidth();
    var time = new Date();
    time = time.getTime();
    var pos = widthCanvas * ((time % this.vitesse) / this.vitesse);
    this.posx = (this.posBase + pos) % widthCanvas;
  };



	this.act = function(env)
	{
		this.move(env);
		this.draw(env);
	};
}







