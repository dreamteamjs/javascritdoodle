function Environment(canvas,gameEngine)
{
	this.gameEngine = gameEngine;
	this.canvas = canvas;
	this.player = new Player(true,gameEngine.nom);
	this.players = [];
	this.arrayItem = []; // tableau de tout les elements presents sur la map
	this.posyA = 1; // offset , afin de maintenir le joueur local dans le canvas
	this.ctxCanvas = canvas.getContext("2d");
	this.ctxCanvas.font = "30px Arial";
	this.ctxCanvas.fillText("Press Enter !", this.canvas.width / 2.7, this.canvas.height / 1.3);
	this.delta = 0;
	this.pointVictory;

	
	this.getContextCanvas = function()
	{
		return this.ctxCanvas;
	};

	this.restart = function()
	{
		if (this.player.posy < this.pointVictory)
		{
			this.printWinning();
		}
		else if (this.player.dead)
		{
			this.printGameOver();
		}
		this.player = new Player(true,this.player.name);

		this.delta = 0;
		this.posyA = 1;
	}


	// fait en sorte que le joueur reste dans le canvas
	this.autoScroll = function()
	{
		if (this.player.posy + this.posyA < 50)
		{
			this.delta+=4;
			this.posyA += this.delta;
		}

		if (this.player.posy + this.posyA < 100)
		{
			this.delta+=2;
			this.posyA += this.delta;
		}

		if (this.player.posy + this.posyA < 200)
		{
			this.delta++;
			this.posyA += this.delta;
		}
		this.delta = 0;
	};

	/*
		definition de la carte passée en parametre
	*/
	this.setMap = function(strMap)
	{
		var tabMap = strMap.split('\n');
		var line;
		var x , y , w , h , color,type,num;
		for (var i = 0 ; i < tabMap.length ; i++)
		{
			if (line == ''){break;}
			line = tabMap[i].split(';');
			x = parseInt(line[0]);
			y = parseInt(line[1]);
			w = parseInt(line[2]);
			h = parseInt(line[3]);

			color = line[4];
			type = line[5];

			
			switch (type)
			{
				case "O": // Obstacle
				{
					var sens = line[6].charCodeAt(0);

					if (sens == 42) // * , qui ne bouge pas
					{
						this.arrayItem.push(new Obstacle(x,y,w,h,color));
					}
					else if (sens == 120) // qui est mouvant sur x
					{
						var timeAllerRetour = parseInt(line[7]);
						var valeurDecalage = parseInt(line[8]);
						this.arrayItem.push(new ObstacleMouvantX(x,y,w,h,color,timeAllerRetour,valeurDecalage,false));
					}
					else if (sens == 121) // qui est mouvant sur y
					{
						var timeAllerRetour = parseInt(line[7]);
						var valeurDecalage = parseInt(line[8]);
						this.arrayItem.push(new ObstacleMouvantY(x,y,w,h,color,timeAllerRetour,valeurDecalage,false));
					}
					else if (sens == 89) // qui est mouvant sur Y , en mode oneshot (un seul saut depuis la plateforme)
					{
						var timeAllerRetour = parseInt(line[7]);
						var valeurDecalage = parseInt(line[8]);
						this.arrayItem.push(new ObstacleMouvantY(x,y,w,h,color,timeAllerRetour,valeurDecalage,true));
					}
					else if (sens == 88) // qui est mouvant sur X , en mode oneshot (un seul saut depuis la plateforme)
					{
						var timeAllerRetour = parseInt(line[7]);
						var valeurDecalage = parseInt(line[8]);
						this.arrayItem.push(new ObstacleMouvantX(x,y,w,h,color,timeAllerRetour,valeurDecalage,true));
					}
					
					break;
				}
				case "J" : // Jeton
				{ 
					num = parseInt(line[6]); // numero du pouvoir
					this.arrayItem.push(new Jeton(x,y,w,h,color,num,this.gameEngine.controleur)); break;
				}

				case "C":
				{ // Lanceur de couteaux (boules de feu au final)
					var deltaLancer = parseInt(line[6]);
					var vitesseCouteau = parseInt(line[7]);
					this.arrayItem.push(new LanceurCouteau(y,this,deltaLancer,vitesseCouteau));
					break;
				}

				case "c":
				{ // Couteau simple (boule de feu)
					if (DEBUG) {console.log("c",x,y);}
					this.arrayItem.push(new Couteau(x,y,0));
					break;
				}
			}
			
		}

		this.pointVictory = y - 20; // victoire , si le joueur dépasse pointVictory

	};
	
	this.getPlayer = function()
	{
		return this.player;
	}
	
	this.getPosXPlayer = function()
	{
		return this.player.getX();
	};
	
	this.getPosYPlayer = function()
	{
		return this.player.getY();
	};
	
	this.getCanvasWidth = function()
	{
		return this.canvas.width;
	};
	
	this.getCanvasHeight = function()
	{
		return this.canvas.height;
	}

	this.printGameOver = function()
	{
		var imgGO = libImg.getImage("gameOver");
		this.ctxCanvas.drawImage(imgGO,0,0,this.getCanvasWidth(),this.getCanvasHeight());
	}

	this.printBeginning = function()
	{
		var imgBegin = libImg.getImage("beginning");
		this.ctxCanvas.drawImage(imgBegin,0,0,this.getCanvasWidth(),this.getCanvasHeight());
	}

	this.printWinning = function()
	{
		var win = libImg.getImage("winning");
		this.ctxCanvas.drawImage(win,0,0,this.getCanvasWidth(),this.getCanvasHeight());
	}
	
	this.clearCanvas = function()
	{
		this.ctxCanvas.clearRect(0, 0, this.getCanvasWidth(), this.getCanvasHeight());
		this.ctxCanvas.drawImage(libImg.getImage("fontGame"),0,0,this.getCanvasWidth(),this.getCanvasHeight());

	}
}