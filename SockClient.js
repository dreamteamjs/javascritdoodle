/*
	Object permettant au client de dialoguer avec le serveur
*/

function SockClient(name,controleur)
{
	this.name = name;
	this.socket = 0;
	this.controleur = controleur;
	
	/*
	Notifie l'etat de l'utilisateur du pc courant à tout les autres clients
	*/
	this.sendState = function(event,posy,posx)
	{
		this.socket.emit('SendUpdateState',this.name,event,posy,posx);
	};
	
	this.init = function()
	{
		this.socket = io.connect();
		sock = this.socket;
		name = this.name;
		controle = this.controleur;
		
			// on connection to server, ask for user's name with an anonymous callback
		this.socket.on('connect', function()
		{	
			// call the server-side function 'adduser' and send one parameter (value of prompt)	
			sock.emit('adduser', name);
		});
		
		this.socket.on('allusers', function(listOfUsers)
		{
			for(var nom in listOfUsers)
			{
				console.log("add");
				console.log(nom);
				controle.newUser(nom); // MAJ du nouveau client , il obtient la liste de tout les clients
			}
		});

		// appellé quand un utilisateur a changé d'etat
		// (deplacement à gauche / droite , arret deplacement à gauche / droite , nouveau pouvoir)
		this.socket.on('RecUpdateState', function (username, event,posy,posx) 
		{
			// username a changé son état
			controle.recState(username,event,posy,posx);
		});
		
		this.socket.on('nwuser',function(username)
		{
			controle.newUser(username);
		});
		
		this.socket.on('deluser',function(username)
		{
			controle.delUser(username);
		});

		this.socket.on('map', function(strMap)
		{
			controle.setMap(strMap);
		});

		this.socket.on('powerusers',function(strMapUserPower)
		{
			controle.setPowerUsers(strMapUserPower);
		});
	};


}