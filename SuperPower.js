function SuperPower(isAuto,x,y,controleur,name)
{
	this.etat = isAuto; // true == automatique, false == manuelle
	this.controleur = controleur;
	this.x = x; // position x du pouvoir
	this.y = y; // position y du pouvoir
	this.consume = false; // si le le jeton a été consommé ou non
	this.donne = false; // si le pouvoir a été capturé ou non
	this.name = name; // nom du pouvoir

	this.apply = function(env,appel) {}

	this.isDonne = function()
	{
		return this.donne;
	}

	this.draw = function(env)
	{}	

	this.abord = function(env)
	{
		this.consume = true;
		this.donne = false;
	}
}