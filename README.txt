﻿#### Projet JS 2016 , Doodle Jump , développeurs {Dib Dominique && Giangrasso Rémy}; ####

Problemes rencontrés:
        - probleme avec le chargement de certaines images sur Safari et chrome , (fonctionnel sur Firefox 44.0.2)
        - mettre en place les sprites
        - le temps  (Améliorer le jeu: son, etat Debut fin jeu, oubli du multiroom, amelioration globale du jeu , Refactor necessaire sur certaines parties ...)
        - prob de synchro -> nous a amené à mettre la prédiction des coups.


Point positif:
        calcul des collisions sur les objets visibles dans le canvas STRICTEMENT
        bonne synchro au final
        architecture solide (Séparation des responsabilités)
        l'ensemble des items de la map sont chargés une seule fois

Type de pouvoir:
        Un pouvoir est automatique quand il n'attend pas une intervention de la part de l'utilisateur pour s'enclencher.
        Un pouvoir est manuel quand il attend une intervention de la part de l'utilisateur , ladite intervention -> (barre espace pressée).

        Jump(Ressort) : simple saut -> automatique
        Super Jump(Rocket) : succession de sauts actionnables manuellement
        PowerLife (Champignon) : augmente dans une certaine proportion les dimensions du joueur automatiquement && si
                le joueur touche le bottom du canvas, il aura droit a un jump automatique pour tenter de rester dans la partie.

Type obstacle :
        Obstacle solide : brique marron
        Obstacle mouvant : brique marron
        Obstacle mouvant oneShoot: des sprites qui disparaissent une fois que le joueur a sauté dessus
        Feu (classe Couteau) : des objets qui lors d'une collision avec un joueur tuent ce dernier.



Règle du jeu:
        Le joueur saute tout le temps sur des obstacles solides, mouvants ou fixes.
        la barre du haut affiche sa position par rapport aux autres joueurs et par rapport à son record, il gagne lorsqu'il atteint le sommet.
        Lorsque le joueur est en collision avec un jeton(contenant pouvoirs),
        Il va attacher ce pouvoir à lui, il pourra le consommmer manuellement (touche      espace pour le rocket) ou automatiquement(comme le champignon ou le        jump(ressort)) .
        Si le joueur possède déjà un pouvoir et passe sur un jeton, il perd ce pouvoir et récupère le pouvoir du jeton.
        Si le joueur tombe ou touche une boule de feu, il meurt.

Conception:
        Tous les pouvoirs héritent de SuperPower.
        GameEngine et PhysicEngine font la règle du jeu.
        SocketClient dialogue avec le serveur.
        ControlEngine sert d'interface entre SocketClient et GameEngine et traite les entrées/sorties
        L'environnement stocke tout ce qui s'affiche sur le canvas , joueur , arrayItems (Jetons -> pouvoir , Obstacles) , et les autres joueurs connectés.
        LibrairieImage et Sound chargement toutes les ressources necessaires. chargement effectué dans le onLoad().
        ScoreBoardManager gere le canvas dédié à l'affichage des positions des joueurs , du record , et du joueur host.




Lancement du jeu:
Il faut être connecté à internet :o), via l'invite de commande se positionner dans le répertoire du jeu et entrer la commande:
node server.js

le port d'ecoute est 8080