function LanceurCouteau(y,env,multipleN,vitesseCouteauMs)
{
	this.init = function(env)
	{
		var delta = 1 / this.nbCouteaux;
		var widthCanvas = env.getCanvasWidth();

		for (var i = 0 ;i < this.nbCouteaux ;i++)
		{
			this.arrayCouteau[i] = new Couteau(widthCanvas * (delta * i) ,this.posy,vitesseCouteauMs);
		}
	}
	
	// multipleN -> A toutes les secondes multiples de multipleN , lancer un couteau
	// vitesseCouteauMs -> temps de parcours d'un couteau dans le canvas.
	Obstacle.call(this,-10,y,1,1,"black");
	this.arrayCouteau = [];
	this.vitesseCouteau = vitesseCouteauMs;
	this.multipleN = multipleN; // a tout les multiples de deltaN , lancer un couteau.
	this.nbCouteaux = Math.trunc(this.vitesseCouteau / this.multipleN);
	this.init(env);



	this.act = function(env)
	{
		for(var i = 0 ; i < this.nbCouteaux ;i++)
		{
			this.arrayCouteau[i].act(env);
		}

		this.draw(env);
	}

	this.getCouteaux = function()
	{
		return this.arrayCouteau;
	}
}