var game;
var leftOnRun,rightOnRun, upOnRun;
var startJeux = false; // etat du jeu
var boutonStart; // recupéré dans le DOM
var boutonReset; //   "       "    "  "
var DEBUG = false; // debug
var PRINTNOHOST = false; // pour le debug
var libImg; // obj stockant toutes les images
var libSound; // obj stockant tout les sons
var PRINTETATRECEIVESOCKET = false; // debug

/*
	On charge toutes les images
*/
window.onload = function()
{
	libImg = new LibrairieImage();
	libImg.init();

	libSound = new LibrairieSound();
	libSound.init();

	var name = prompt("What's your name ?");
	game = new GameEngine(name);
	game.start();

	mainloop();
}

/*
	Moteur du jeu , affichage du game over , de la partie ...
*/

function GameEngine(nom)
{
	this.nom = nom;
	this.scoreBoard = undefined;
	this.mapLoaded = false;
	this.context = undefined;
	this.moteurPhysique = undefined;
	this.controleur = undefined;

	/*
		demarrage du jeu
	*/
	this.start = function()
	{
		var canvas = document.getElementById("myCanvas");
		var canvasScoreBoard = document.getElementById("ScoreBoard");
		boutonStart = document.getElementById("buttonStart");
		boutonReset = document.getElementById("buttonReset");
		this.context = new Environment(canvas,this);
		this.scoreBoard = new ScoreBoardManager(canvasScoreBoard);
		this.controleur = new GameControleur(this);
		this.moteurPhysique = new PhysicEngine(this.arrayItem,this);
		
		document.addEventListener('keydown',toucheDown,false);
		document.addEventListener('keyup',toucheUp,false);
		boutonStart.addEventListener('mousedown', onClique);
		boutonReset.addEventListener('mousedown',onClickRestart);
		boutonReset.style.visibility = 'hidden';
		this.context.printBeginning();
	}

	/*
		On recommence une partie
	*/
	this.restart = function()
	{
		this.context.clearCanvas();
		this.context.restart();
		this.reInitializeObstacle();
		this.moteurPhysique.restart();
		this.scoreBoard.clear();
		startJeux = false;
		this.controleur.sendState("reborn",this.context.player.posy,this.context.player.posx);
		boutonStart.style.visibility = 'visible';
	}

	this.setMap = function(map)
	{
		this.context.setMap(map);
		lastObj = this.context.arrayItem[this.context.arrayItem.length - 1];
		if (DEBUG) {console.log(lastObj);}
		this.scoreBoard.setMaxY(Math.abs(lastObj.posy) + 25);
		this.mapLoaded = true;
	}

	/*
		appellée au demarrage de la session du joueur local , synchro sur les pouvoirs acquis par 
		les autres joueurs.
	*/
	this.setPowerUsers = function(map)
	{
		var splitmap = map.split("[");
		for (var i = 0 ; i < splitmap.length ; i++)
		{
			var splitline = splitmap[i].split("]");
			var player = this.context.players[splitline[0]];
			var pouvoir = splitline[1];

			switch (pouvoir)
			{
				case "powerattach-PowerSuperJump":
				{
					if (DEBUG) {console.log("this player get powersuperjump",player)};
					var power = new PowerSuperJump(false,0,0,this.controleur,player);
					power.donne = true;
					player.activeSuperPower = power;
					break;
				}

				case "powerattach-PowerLife":
				{
					var power = new PowerLife(true,0,0,this.controleur,player);
					power.donne = true;
					player.activeSuperPower = power;
					break;
				}
			}
		}
	}

	/* boucle principale du jeu */
	this.action = function()
	{
		this.scoreBoard.clear(); // clean screen

		var player = this.context.player;
		if (DEBUG) {console.log("LENGTH",this.context.players.length);}

		if(startJeux && this.mapLoaded) // si le jeu est en cours (true) et carte chargée
		{
			this.context.clearCanvas();
			player.apply(this.context);
			player.move(this.context);

			this.scoreBoard.print(player,true);

			if (player.isDead()) // SI JOUEUR MORT
			{
				libSound.getSound("endGame").play();
				boutonReset.style.visibility = 'hidden';
				this.restart();
				return;
			}

			 // CONDITION VICTOIRE

			if (this.context.pointVictory > player.posy )
			{
				libSound.getSound("victoire").play();
				boutonReset.style.visibility = 'hidden';
				this.restart();
				return;
			}

			player.draw(this.context);
			this.context.autoScroll();
			this.moteurPhysique.update(this.context,player);
		
			this.moveOtherPlayer();
		}
		
	};
	
	/*
		Faire bouger tout les autres joueurs. prédiction des collisions et de l'activation du pouvoir
	*/
	this.moveOtherPlayer = function()
	{
		for(var p in this.context.players)
		{
			var player = this.context.players[p];
			player.act(this.context);
			if (DEBUG) {console.log("ISHOST:",player.host);}
			this.moteurPhysique.update(this.context,player);
			this.scoreBoard.print(player,false);

		}
	}

	/*
		recupere les changements d'etats des autres joueurs
	*/
	this.changeStateFor = function(username,state,posy,posx)
	{
		var posxint = parseInt(posx);
		var posyint = parseInt(posy);
		if (!(username in this.context.players)) {return;}

		var player = this.context.players[username];
		player.posy = posyint;
		player.posx = posxint;

		if (PRINTETATRECEIVESOCKET) {console.log(state);}
			// les etats
		switch(state)
		{	
			case "up": player.speedX = 0; break;

			case "reborn":
			{
				this.context.players[username] = new Player(false);
				break;
			}

			case "keyspaceDown" :
			{
				if (player.activeSuperPower != undefined && player.activeSuperPower.apply(game.context,false))
				{
					player.activeSuperPower.abord(this.context);
					player.activeSuperPower = undefined;
				}
				break;
			}

			case "powerattach-PowerJump":
			{
				var power = new PowerJump(true,posxint,posyint,this.controleur);
				power.donne = true;
				player.activeSuperPower = power;
				break;
			}

			case "powerattach-PowerSuperJump":
			{
				var power = new PowerSuperJump(false,posxint,posyint,this.controleur,player);
				power.donne = true;
				player.activeSuperPower = power;
				break;
			}

			case "powerattach-PowerLife":
			{
				var power = new PowerLife(true,posxint,posyint,this.controleur,player);
				power.donne = true;
				player.activeSuperPower = power;
				break;
			}

			case "killpower":
			{
				if (player.activeSuperPower != undefined)
				{
					player.activeSuperPower.abord(this.context);
					player.activeSuperPower = undefined;
				}
				break;
			}

			case "gaucheKeyDown": player.speedX = -5; player.goLeft = true; break;

			case "droiteKeyDown": player.speedX = 5; player.goLeft = false; break;
	
			default: break;
		}
	}

	this.getPlayer = function()
	{
		return this.context.player;
	}
	
	/*
		Nouvel utilisateur connecté
	*/
	this.addUser = function(username)
	{
		if (DEBUG) {console.log("nw-user",username);}
		nwplayer = new Player(false,username);
		this.context.players[username]=nwplayer;
	}
	
	/*
		un utilisateur se deconnecte
	*/
	this.delUser = function(username)
	{
		delete this.context.players[username];
	}

	/*
		On reinitialise tout les elements de la map
	*/
	this.reInitializeObstacle = function()
	{
		for(var i = 0; i < this.context.arrayItem.length; i++)
		{
			this.context.arrayItem[i].reInitialize();
		}
	}
}

var mainloop = function()
{
	game.action();
	requestAnimationFrame(mainloop);
};




