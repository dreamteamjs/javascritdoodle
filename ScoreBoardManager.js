function ScoreBoardManager(canvas)
{
	this.canvas = canvas;
	this.ctxCanvas = canvas.getContext('2d');
	this.maxY = 100000;
	this.record = 0;

	this.clear = function()
	{
		this.ctxCanvas.clearRect(0, 0, this.canvas.width, this.canvas.height);
	}

	this.setMaxY = function(maxY)
	{
		this.maxY = maxY;
	}

	this.printRecord = function() // record local du joueur
	{
		this.ctxCanvas.save();
		this.ctxCanvas.beginPath();	
		this.ctxCanvas.rect(this.record , 0 , 3 , this.canvas.height);
		this.ctxCanvas.fillStyle = "blue";
		this.ctxCanvas.closePath();
		this.ctxCanvas.fill();
		this.ctxCanvas.restore();
	}

	this.print = function(player,itsMe)
	{
		var posXBarre = Math.abs(player.posy - 400) / (this.maxY + 500);
		posXBarre = posXBarre * this.canvas.width;

		if (posXBarre > this.record)
		{
			this.record = posXBarre;
		}

		this.printRecord();

		this.ctxCanvas.save();
		this.ctxCanvas.beginPath();

		if (itsMe)
		{
			this.ctxCanvas.rect(posXBarre, 0, 3,this.canvas.height);
			this.ctxCanvas.fillStyle = "red";
		}
		else
		{
			this.ctxCanvas.rect(posXBarre, 0, 2,this.canvas.height);
			this.ctxCanvas.fillStyle = "green";
		}
		
		this.ctxCanvas.closePath();
		this.ctxCanvas.fill();
		this.ctxCanvas.restore();
	}

}