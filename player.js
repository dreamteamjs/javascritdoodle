function Player(isHost,name)
{
	this.name = name;
	this.posx = 125;
	this.posy = 310;
	this.color = 'green';
	this.width = 28;
	this.height = 30;
	this.speedX = 0;
	this.speedY = 0;
	this.host = isHost;
	this.direction = '';
	this.onJump = false;
	this.dead = false;
	this.activeSuperPower = undefined;

	this.goLeft = false;  // pour l'image
	this.imgDroite = libImg.getImage("personnageRight");
	this.imgGauche = libImg.getImage("personnageLeft");
	
	this.imgGauche.width = 124;   
  	this.imgGauche.height = 120;
	this.imgDroite.width = 124;   
  	this.imgDroite.height = 120;


	
	
	this.act = function(context)
	{
		// on ne fait pas bouger les autres joueurs si ils ne sont pas dans le canvas
		if (!this.isHost() && (this.posy + context.posyA < 0 || this.posy + context.posyA > 400 )) {return;} // No DrawOutOfCanvas
		this.apply(context);
		this.move(context);
		if (PRINTNOHOST)
		{
			console.log(this.posx,this.posy);
		}
		this.draw(context);
	};

	/*
		on tente d'activer un pouvoir si il y en a un
	*/
	this.apply = function(context)
	{
		if(this.activeSuperPower != undefined && this.activeSuperPower.apply(context,true))
		{	
			this.activeSuperPower = undefined;
			if (this.isHost())
			{
				// indiquer à tout les joueurs que le joueur courant a consommé son pouvoir
				context.gameEngine.controleur.sendState("killpower",this.posy,this.posx);
			}
			if (DEBUG) {console.log("Mort du pouvoir");}
		}
	};

	this.attach = function(power)
	{
		// on attache le pouvoir au joueur
		power.donne = true;
		this.activeSuperPower = power;
		power.controleur.sendState("powerattach-".concat(power.name),this.posy,this.posx);
	};
	

	this.draw = function(context)
	{		
		if (this.activeSuperPower != undefined)
		{
			this.activeSuperPower.draw(context);
		}

		ctxCanvas = context.getContextCanvas();
		ctxCanvas.save();
		ctxCanvas.beginPath();
		ctxCanvas.translate(this.posx, this.posy + context.posyA);

		if(this.goLeft)
		{
			ctxCanvas.drawImage(this.imgGauche, -2, -10, this.imgGauche.width / 3, this.imgGauche.height / 3);
		}

		else
		{
			ctxCanvas.drawImage(this.imgDroite, -2, -10, this.imgDroite.width / 3, this.imgDroite.height / 3);
		}

		ctxCanvas.fillStyle = this.color;
		ctxCanvas.fill();
    	ctxCanvas.closePath();
		ctxCanvas.restore();
	};
	
	this.move = function(context)
	{
		this.speedY = this.speedY + 0.5;
		this.posx = (this.posx + this.speedX) % context.getCanvasWidth();
        if(this.posx <= 0)
        {
            this.posx = Math.abs(this.posx) % context.getCanvasWidth();
            this.posx = context.getCanvasWidth() - this.posx;
        }

		this.posy = (this.posy + this.speedY);

		if (this.posy + context.posyA >= context.getCanvasHeight() && this.isHost())
		{
			this.dead = true;
		}
	};
	
	this.jump = function()
	{
		libSound.getSound("soundJump").play();
		this.speedY = -10;
	};

	this.isHost = function()
	{
		return this.host;
	}
	
	this.getPosX = function()
	{
		return this.posx;
	};
	
	this.getPosY = function()
	{
		return this.posy;
	};

	this.getDirection = function()
	{
		return this.direction;
	};

	this.getOnJump = function()
	{
		return this.onJump;
	};
	

	this.setOnJump = function(b)
	{
		this.onJump = b;
	};
	
	this.isDead = function()
	{
		return this.dead;
	};

	this.killPower = function(env) // SEULEMENT APPELLE PAR HOST !!
	{
		if (this.activeSuperPower != undefined)
		{
			var controle = this.activeSuperPower.controleur;
			controle.sendState("killpower",this.posy,this.posx);
			this.activeSuperPower.abord(env);		
		}
		this.activeSuperPower = undefined;
	};	
	
}
