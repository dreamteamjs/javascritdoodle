/*
	Objet dédié au chargement et au stockage des images du jeu
*/
function LibrairieImage()
{
	this.tabImage = [];
	
	this.init = function()
	{
		var imgPersoLeft = new Image();
		imgPersoLeft.src = "http://i.imgur.com/J29LH2M.png"; 
		this.tabImage["personnageLeft"] = imgPersoLeft;

		var imgPersoRight = new Image();
		imgPersoRight.src = "http://imgur.com/bgAXhtp.png";
		this.tabImage["personnageRight"] = imgPersoRight;

		var imgObstacle = new Image();
		imgObstacle.src = "http://imgur.com/QBavf8k.png";
		this.tabImage["obstacle"] = imgObstacle;

		var spriteFeu = new Image();
		spriteFeu.src = "http://i.imgur.com/98anYLB.png"; 
		this.tabImage["feu"] = spriteFeu;

		var spriteRocket = new Image();
		spriteRocket.src = "http://imgur.com/y9MpfOA.png";
		this.tabImage["rocket"] = spriteRocket;

		var spriteJump = new Image();
		spriteJump.src = "http://imgur.com/eqdLN1t.png"; 
		this.tabImage["jump"] = spriteJump;

		var imgChampi = new Image();
		imgChampi.src = "http://imgur.com/XbTAlHb.png";
		this.tabImage["champi"] = imgChampi;

		var imgObstacleMouvant = new Image();
		imgObstacleMouvant.src = "http://imgur.com/suLX0Gb.png";
		this.tabImage["obsMouvant"] = imgObstacleMouvant;

		var imgGameOver = new Image();
		imgGameOver.src = "gameover.jpg";
		this.tabImage["gameOver"] = imgGameOver;

		var beginning = new Image();
		beginning.src = "beginning.jpg";
		this.tabImage["beginning"] = beginning;

		var fontgame = new Image();
		fontgame.src = "font.jpg";
		this.tabImage["fontGame"] = fontgame;

		var win = new Image();
		win.src = "winning.jpg";
		this.tabImage["winning"] = win;

	};

	this.getImage = function(img) {
		return this.tabImage[img];
	};
	



}