/*
	JETON -> stocke un pouvoir (instantiation dans setPower()) , 
											chaque pouvoir est identifié par un 'num'

*/

function Jeton(x,y,w,h,color,num,controleur)
{
	Obstacle.call(this,x,y,w,h,color);

	this.setPower = function()
	{
		if (DEBUG) {console.log("Y JETON",this.posy);}
		var player = controleur.environment.player;
		switch(this.num)
		{
			case 0: this.pouvoir = new PowerJump(true,this.posx,this.posy,controleur,player); break;  // auto
			//case 1: this.pouvoir = new PowerJump(false,controleur); break;  // manuelle
			case 2: this.pouvoir = new PowerLife(true,this.posx,this.posy,controleur,player); break;
			case 3: this.pouvoir = new PowerSuperJump(true,this.posx,this.posy,controleur,player); break;
			case 4: this.pouvoir = new PowerSuperJump(false,this.posx,this.posy,controleur,player); break;
			default : break;
		}
	}

	////////////////// OVERRIDE MTH ///////////////////////////////
	this.reInitialize = function()
	{
		this.setPower();
	}
	///////////////////////////////////////////////////////

	////////////////////////////////////////////////// CONSTRUCTEUR ////////////////////

	this.pouvoir = undefined;
	this.num = num;
	this.reInitialize();

	//!!!!!!!!!!!!!!!!!!/////////////////////////////////////////////////////////!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	this.actionTop = function()
	{}
	
	
	this.draw = function(context)
	{
		if (!this.pouvoir.donne || this.pouvoir.consume)
		{this.pouvoir.draw(context);}
	}
		
}