/*
	Fait le lien entre la couche reseau et le gameEngine , filtre les messages en eliminant les incoherences
*/

function GameControleur(gameEngine)
{
	this.gameEngine = gameEngine;
	this.environment = gameEngine.context;
	this.sock = new SockClient(gameEngine.nom,this);
	this.sock.init();
	
	this.newUser = function(username)
	{
		if (username != this.gameEngine.nom) // elimination d'une incoherence
		{
			console.log("new user!");
			this.gameEngine.addUser(username);
		}
		
	};
	
	this.delUser = function(username)
	{
		if (username != this.gameEngine.nom)
		{
			this.gameEngine.delUser(username);
		}
		
	};

	this.setMap = function(strMap)
	{
		if (this.environment.arrayItem.length == 0)
		{
			this.gameEngine.setMap(strMap);
		}
		
	};

	this.setPowerUsers = function(map)
	{
		this.gameEngine.setPowerUsers(map);
	}
	
	this.sendState = function(etat,posy,posx)
	{
		this.sock.sendState(etat,posy,posx);
	};
	
	this.recState = function(username,etat,posy,posx)
	{
		if (username != this.gameEngine.nom) // exemple d'incoherence
		{
			this.gameEngine.changeStateFor(username,etat,posy,posx);
		}

	};
}

function toucheUp(event) // clavier , touche relachée
{
		if (event.which == 37)
		{
			leftOnRun = false;
		}
		else if (event.which == 39)
		{
			
			rightOnRun = false;
		}
		
		if (!(rightOnRun || leftOnRun))
		{
			game.getPlayer().speedX = 0;
			game.controleur.sendState("up",game.getPlayer().posy.toString(),game.getPlayer().posx.toString());
		}
};

function toucheDown(event) // clavier , touche enfoncée
{
		if(event.which == 13 && startJeux == false) {  // Enter
			startJeux = true;
			boutonStart.style.visibility = 'hidden';
			boutonReset.style.visibility = 'visible';
			libSound.getSound('startGame').play();
		}	

		if (event.which == 37)
		{
			if (!leftOnRun)
			{
				game.controleur.sendState("gaucheKeyDown",game.getPlayer().posy.toString(),game.getPlayer().posx.toString());
			}
			leftOnRun = true;
			game.getPlayer().goLeft = true;
			game.getPlayer().speedX = -5;
		}

		else if (event.which == 39)
		{
			if (!rightOnRun)
			{
				game.controleur.sendState("droiteKeyDown",game.getPlayer().posy.toString(),game.getPlayer().posx.toString());
			}
			rightOnRun = true;
			game.getPlayer().goLeft = false;
			game.getPlayer().speedX = 5;
		}

		else if (event.which == 32)
		{
			// si le joueur a un pouvoir && si il s'active manuellement
			if (game.getPlayer().activeSuperPower != undefined && game.getPlayer().activeSuperPower.apply(game.context,false))
			{
					if (DEBUG) {console.log("touchDown");}

					game.controleur.sendState("killpower",this.posy,this.posx);
					game.getPlayer().activeSuperPower = undefined;
			}
			game.controleur.sendState("keyspaceDown",game.getPlayer().posy.toString(),game.getPlayer().posx.toString());
		}
};

function onClique(event)// (onClickStart)
{
	if(event.which == 1)
	{
		startJeux = true;
		libSound.getSound('startGame').play();
		boutonReset.style.visibility = 'visible';
		boutonStart.style.visibility = 'hidden';
	}	
};

function onClickRestart(event)
{
	if (startJeux)
	{
		game.restart();
		libSound.getSound('startGame').play();
		startJeux = true;
		boutonStart.style.visibility = 'hidden';
	}
	
}