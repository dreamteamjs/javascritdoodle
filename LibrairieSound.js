function LibrairieSound()
{
	this.tabSound = [];
	
	this.init = function()
	{
		var audioExplode = new Audio('explosionObstacle.mp3');
		this.tabSound["explosionObstacle"] = audioExplode;

		var soundJump = new Audio('soundJump.mp3');
		this.tabSound["soundJump"] = soundJump;

		var acquire = new Audio('acquireItem.mp3');
		this.tabSound["acquireItem"] = acquire;

		var endSound = new Audio('endGame.mp3');
		this.tabSound["endGame"] = endSound;

		var victoire = new Audio('victoire.mp3');
		this.tabSound["victoire"] = victoire;

		var startSound = new Audio('startGame.mp3');
		this.tabSound['startGame'] = startSound;
	}

	this.getSound = function(titleSound)
	{
		return this.tabSound[titleSound];
	}
}