function PowerLife(isAuto,x,y,controleur,player)
{
	SuperPower.call(this,isAuto,x,y,controleur,"PowerLife");  // herite de SuperPower
	this.oneShotResizeDoodle = true;
	this.img = libImg.getImage("champi");

  	this.offset = 0;
  	this.currentFrame = 0;
  	this.then = performance.now();
  	this.totalTimeSinceLastRedraw = 0;
  	this.delayBetweenFrames = 1000 / 20;  // delay in ms between images



	this.player = player;

	this.apply = function(env,appel)
	{
		if (this.etat == appel)
		{
			var player = this.player;

			if (this.oneShotResizeDoodle)
			{
				player.imgDroite.width *= 1.3;
      			player.imgDroite.height *= 1.3;
      			player.imgGauche.width *= 1.3;
      			player.imgGauche.height *= 1.3;
      			player.width *= 1.5;
      			player.height *= 1.5;
      			this.oneShotResizeDoodle = false;
			}

			
			var tmpSpeedY = player.speedY + 1;
			var tmp = player.posy + tmpSpeedY - player.height + env.posyA;

			if(tmp >= env.getCanvasHeight() - player.height)
			{	
				this.controleur.sendState("up",player.posy.toString(),player.posx.toString()); 
				player.width /= 1.5;
      			player.height /= 1.5;
      			player.imgDroite.width /= 1.3;
      			player.imgGauche.width /= 1.3;
    		  	player.imgDroite.height /= 1.3;
    		  	player.imgGauche.height /= 1.3;
				player.jump();
				this.consume = true;
				return true;
			}
		}
		this.consume = false;
		return false;
	};

	this.draw = function(env)
	{ 
		if(this.consume) { return ;}	
    	//*\\//*\\//*\\ DRAW //*\\//*\\//*\\

    	if(!this.isDonne())
    	{
    		var scale = 4;
    		ctxCanvas = env.getContextCanvas();
			ctxCanvas.save();
			ctxCanvas.beginPath();
			ctxCanvas.translate(this.x, this.y + env.posyA);
    		ctxCanvas.drawImage(this.img, 0, 0, this.img.width, this.img.height, 0, 0, 32, 32);

    		ctxCanvas.fillStyle = this.color;
	    	ctxCanvas.fill();  
	    	ctxCanvas.closePath();
	    	ctxCanvas.restore();
    	}
    //*\\//*\\//*\\ DRAW //*\\//*\\//*\\

	}

	// desactivation du pouvoir , remise en place de la taille du player
	this.abord = function(env)
	{
		var player = this.player;
		player.width /= 1.5;
      	player.height /= 1.5;
		player.imgDroite.width /= 1.3;
		player.imgGauche.width /= 1.3;
	  	player.imgDroite.height /= 1.3;
	  	player.imgGauche.height /= 1.3;
		this.donne = false;
		this.consume = true;
	}

}