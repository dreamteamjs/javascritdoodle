/*
	Moteur physique -> detection des collisions
*/

function PhysicEngine(arrayItem,gameEngine)
{
	this.arrayItem = arrayItem;
	this.indiceObstacle = 0;
	this.gameEngine = gameEngine;
	this.controleur = gameEngine.controleur;
	this.environment = gameEngine.context;

	this.restart = function()
	{
		this.indiceObstacle = 0;
	};

	/*
		update dessine UNIQUEMENT ce qui entre dans le canvas
	*/
	this.update = function(env,player)
	{
		var arrayItem = this.environment.arrayItem;

		for(var i = this.indiceObstacle - 1; i > - 1; i--) // EN BAS
		{
			if(! this.testCollision(env,i,true,player))
			{
				break;
			}
		}

		for (var i = this.indiceObstacle; i < arrayItem.length; i++)
		{	
			if(!this.testCollision(env,i,false,player)) // EN HAUT
			{
				break;
			}
		}
	};

	/*
		Test de la collision , return true si l'item courant 'o' ne sort pas du canvas
	*/
	this.testCollision = function(env,i,boolMontee,myPlayer)
	{	
		var arrayItem = this.environment.arrayItem;
		var o = arrayItem[i];
		
		if (!myPlayer.isHost() && (myPlayer.posy + env.posyA > 400 || myPlayer.posy + env.posyA < 0))
		{
			// si myPlayer sort du canvas return false
			return false;
		}

		if (boolMontee && o.posy + env.posyA > 400) // si je regarde ceux du bas
		{
			// si on sort du canvas return false
			return false;
		}
		//console.log("POSY+A");
		//console.log(o.posy + env.posyA);
		if (!boolMontee && o.posy + env.posyA < 0) // ceux du haut
		{
			return false;
		}


		if (myPlayer.isHost()) // si myPlayer est le joueur local -> actionner l'obstacle
		{
			o.act(env);
		}
		

		// si 'o' est un Lanceur de couteaux et myPlayer joueur local , tester la collision avec les couteaux
		if (o instanceof LanceurCouteau && myPlayer.isHost())
		{
			var lesCouteaux = o.getCouteaux();
			var c;
			for(var i = 0 ; i < lesCouteaux.length ;i++)
			{
				c = lesCouteaux[i];
				if (rectRectsOverlap(c.posx , c.posy + env.posyA , c.width , c.height , myPlayer.posx , myPlayer.posy + env.posyA,myPlayer.width, myPlayer.height))
				{
					myPlayer.dead = true;
				}
			}
		}

		if (rectRectsOverlap(o.posx, o.posy + env.posyA, o.width, o.height, myPlayer.posx, myPlayer.posy + env.posyA, myPlayer.width, myPlayer.height))
		{
			if(o instanceof Jeton && !(o.pouvoir.donne || o.pouvoir.consume) && myPlayer.isHost())
			{
				// Acquisition d'un pouvoir

				libSound.getSound("acquireItem").play();
				if(this.gameEngine.getPlayer().activeSuperPower == undefined)
				{
					this.gameEngine.getPlayer().attach(o.pouvoir);
				}

				else
				{
					this.gameEngine.getPlayer().killPower(env);
					this.gameEngine.getPlayer().attach(o.pouvoir);
				}
				
			}

			if (o instanceof Couteau) // si collision avec un couteau
			{
				myPlayer.dead = true;
			}

			else if(o.touchOnTop(myPlayer))
			{
				this.indiceObstacle = i;  // memorise
				o.actionTop(myPlayer);
				if (!myPlayer.isHost()) {return false;}
				//this.controleur.sendState("jump",myPlayer.posy.toString(),myPlayer.posx.toString());
			}
		}
		return true;

		
	};
}

// Fonction dédiée au calcul de la collision
function rectRectsOverlap(x0, y0, w0, h0, x1, y1, w1, h1)
{
	return (x0 < x1 + w1 && 
			x0 + w0 > x1 &&
			y0 < y1 + h1 &&
			y0 + h0 > y1);
}

