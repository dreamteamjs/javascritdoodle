
// We need to use the express framework: have a real web servler that knows how to send mime types etc.
var express=require('express');

var fs = require('fs');

// usernames which are currently connected to the chat
var usernames = {};
var userPower = {};

var dataObstacles = "";

fs.readFile('lesObstacles.txt' , function(err,data)
{
	if (err)
	{
		return console.error(err);
	}
	dataObstacles = data.toString();
});

// Init globals variables for each module required
var app = express()
  , http = require('http')
  , server = http.createServer(app)
  , io = require('socket.io').listen(server);

// launch the http server on given port
server.listen(8080);

// Indicate where static files are located. Without this, no external js file, no css...  
app.use(express.static(__dirname + '/'));    

// routing
app.get('/', function (req, res)
{
  res.sendfile(__dirname + '/doodleJmp.html');
});



io.sockets.on('connection', function (socket)
{

	socket.on('SendUpdateState', function (userName,event,posy,posx)
	{
		io.sockets.emit('RecUpdateState',userName,event,posy,posx);
		if (event.length > 5 && event.substring(0,5) == "power")
		{
			userPower[userName] = event;
		}
		else if(event == "killpower")
		{
			if (userName in userPower) { delete userPower[userName];}
		}
	});
	
	
	socket.on('adduser', function(username)
	{
		console.log(username);
		socket.emit('map',dataObstacles);
		socket.emit('allusers',usernames);
		var strbuild = "";
		for (var user in userPower)
		{
			strbuild = strbuild.concat(user).concat("]").concat(userPower[user]).concat("[");
		}

		socket.emit('powerusers',strbuild);
		socket.username = username;
		usernames[username] = username;
		io.sockets.emit('nwuser', username);
		console.log(usernames);
	});


	socket.on('disconnect', function()
	{
		console.log("delete server :",usernames[socket.username]);
		delete usernames[socket.username];
		if (socket.username in userPower) {delete userPower[socket.username];}
		
		io.sockets.emit('deluser', socket.username);
	});
});