function ObstacleMouvantY(x,y,w,h,color,timeAllerRetour,valDecalage,oneShotMode)
{
	Obstacle.call(this,x,y,w,h,color);
	this.time = timeAllerRetour // en millisecondes
	this.aller = this.time / 2.0;
	this.valDecalage = valDecalage;
	this.posyDeBase = y;
	this.isTouched = false;
	this.isDead = false;
	this.oneShotMode = oneShotMode;

	this.spritesheet = libImg.getImage("obsMouvant"); 
  	this.spritesheet.width = 125; 
  	this.spritesheet.height = 37;  // 276 / 7

  	this.offset = 0;
  	this.currentFrame = 0;
  	this.then = performance.now();
  	this.totalTimeSinceLastRedraw = 0;
  	this.delayBetweenFrames = 1000 / 20;  // delay in ms between images


  	this.actionTop = function(player)
	{
		if (this.isDead) // a true uniquement si mode oneshot et plateforme deja utilisée
		{
			return;
		}
		// si mode oneShot , faire disparaitre l'objet
		if (this.oneShotMode)
		{
			this.isTouched = true;
			libSound.getSound("explosionObstacle").play()
		}
		
		player.posy = this.posy - player.height;
		player.jump();

	}
			

	this.move = function()
	{
		var now = new Date();
		now = now.getTime();
		now = Math.abs(this.time - now) % this.time;

		if (now < this.aller)
		{
			this.posy = this.posyDeBase + Math.round((now / this.aller) * this.valDecalage);
		}
		else
		{
			this.posy = (this.posyDeBase + this.valDecalage) - Math.round(((now - this.aller) / this.aller) * this.valDecalage);
		}

	}

	this.draw = function(env)
	{
		 if(this.isDead)
		 {
		 	return;
		 }

		// Use time based animation to draw only a few images per second
    	var now = performance.now();
    	var delta = now - this.then; 
		
		//*\\//*\\//*\\ DRAW //*\\//*\\//*\\ 
		ctxCanvas = env.getContextCanvas()
		ctxCanvas.save();
		ctxCanvas.beginPath();
		ctxCanvas.translate(this.posx, this.posy + env.posyA);
    	
    	if(oneShotMode)
    	{
    		if(this.isTouched)
    		{
    			ctxCanvas.drawImage(this.spritesheet, 0, this.offset, this.spritesheet.width, this.spritesheet.height, 0, 0, 70, 15);
    		}
    		
    		else
    		{
    			ctxCanvas.drawImage(this.spritesheet, 0, 0, this.spritesheet.width, this.spritesheet.height, 0, 0, 70, 15);
    		}
   		}

		else // MODE NON ONESHOT
		{
			ctxCanvas.drawImage(this.image, 0, 0, this.width,this.height);
		}

		ctxCanvas.fillStyle = this.color;
		ctxCanvas.fill();
    
		ctxCanvas.restore();

		if (this.totalTimeSinceLastRedraw > this.delayBetweenFrames && oneShotMode && this.isTouched) {
     
     	 // Go to the next sprite image
	      this.currentFrame++; 
	      this.currentFrame %=  7;  //this.spriteArray.length;  == nbImage
	      this.offset += this.spritesheet.height;
	      
	      if(this.currentFrame % 7 == 0) {
	         this.offset = 0;
	         this.isTouched = false;
	         this.isDead = true;
	         
	       }

	      // reset the total time since last image has been drawn
	      this.totalTimeSinceLastRedraw = 0;
	 	} 

	    else 
	    {
	      // sum the total time since last redraw
	      this.totalTimeSinceLastRedraw += delta;
	    }

	    this.then = now;
	};

	this.reInitialize = function()
	{
		this.isTouched = false;
		this.isDead = false;

	}
}