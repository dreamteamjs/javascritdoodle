/*
	Objet Obstacle
*/

function Obstacle(x,y,w,h,color)
{
	this.posx = x;
	this.posy = y;
	this.width = w;
	this.height = h;
	this.color = color;
  	this.image = libImg.getImage("obstacle");
  	this.image.width = 97; 
  	this.image.height = 32;  

	
	this.act = function(env)
	{
		this.move();
		this.draw(env);
	};
	
	
	this.move = function()
	{
		
	};

	/*
		Action a realiser sur le joueur
	*/
	this.actionTop = function(player)
	{
		player.posy = this.posy - player.height;
		player.jump();
	};

	
	this.draw = function(env)
	{
		ctxCanvas = env.getContextCanvas()
		ctxCanvas.save();
		ctxCanvas.beginPath();
		ctxCanvas.translate(this.posx, this.posy + env.posyA);
    
		ctxCanvas.drawImage(this.image, 0, 0, this.width, this.height);
		//ctxCanvas.rect(0, 0, this.width,this.height);

		ctxCanvas.fillStyle = this.color;
		ctxCanvas.fill();
    
		ctxCanvas.restore();
	};
	
	/*
		Return true/false si l'obstacle a été touché sur le dessus
	*/
	this.touchOnTop = function(player)
	{
		if (player.speedY > 0)
		{
			if (player.posy < this.posy && player.posy - player.speedY < this.posy &&
			 player.posy + player.height - player.speedY - 1 < this.posy)
			{
				return true;
			}

		}
	};

	this.reInitialize = function() {};
}
