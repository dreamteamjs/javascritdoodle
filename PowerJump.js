function PowerJump(isAuto,x,y,controleur)
{
	SuperPower.call(this,isAuto,x,y, controleur,"PowerJump");  // herite de SuperPower

	 // load the spritesheet
 	this.spritesheet = libImg.getImage("jump"); 
  this.spritesheet.width = 60; // 300 = 60 * 5 (nb img)
  this.spritesheet.height = 44;  

  this.offset = 0;
  this.currentFrame = 0;
  this.then = performance.now();
  this.totalTimeSinceLastRedraw = 0;
  this.delayBetweenFrames = 1000 / 20;  // delay in ms between images

	this.apply = function(env,appel) // appliquer le pouvoir , appel indique si appel automatique ou non
	{
		if(this.etat == appel)
		{
			env.getPlayer().jump();
			this.controleur.sendState("up",env.getPlayer().posy.toString(),env.getPlayer().posx.toString());
      		this.consume = true; // pouvoir consommé
			return true; // indique que le pouvoir doit se détacher du player associé
		}
	};


	this.draw = function(env)
	{ 
   // Use time based animation to draw only a few images per second
   //*\\//*\\//*\\ DRAW //*\\//*\\//*\\
	    var now = performance.now();
	    var delta = now - this.then;

    
		ctxCanvas = env.getContextCanvas();
		ctxCanvas.save();
		ctxCanvas.beginPath();
		ctxCanvas.translate(this.x, this.y + env.posyA);
	    
	    if(this.isDonne())
	    {
	      ctxCanvas.drawImage(this.spritesheet, this.offset, 0, this.spritesheet.width, this.spritesheet.height, -13, -20, 32, 32);
	    }

	    else {
	      ctxCanvas.drawImage(this.spritesheet, 240, 0, this.spritesheet.width, this.spritesheet.height,  -13, -20, 32,32);
	    }
    

	    ctxCanvas.fillStyle = this.color;
	    ctxCanvas.fill();  
	    ctxCanvas.closePath();
	    ctxCanvas.restore();
	    //*\\//*\\//*\\ DRAW //*\\//*\\//*\\

	    if (this.totalTimeSinceLastRedraw > this.delayBetweenFrames) {
	      
	       // Go to the next sprite image
	      this.currentFrame++; 
	      this.currentFrame %=  5;  //this.spriteArray.length;  == nbImage
	      this.offset += this.spritesheet.width;
	      
	      if(this.currentFrame % 5 == 0) {
	         this.offset = 0;
	       }
	      //console.log("Offset " + this.offset);  // valeur max offset = 240
	          
	      // reset the total time since last image has been drawn
	      this.totalTimeSinceLastRedraw = 0;
	    } 

	    else {
	      // sum the total time since last redraw
	      this.totalTimeSinceLastRedraw += delta;
	    }

	    this.then = now;
		};


}