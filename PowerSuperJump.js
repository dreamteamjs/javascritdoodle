/*
	Pouvoir donnant droit à l'utilisateur de sauter plusieurs fois (draw d'une fusée)
*/

function PowerSuperJump(isAuto,x,y,controleur,player)
{
	SuperPower.call(this,isAuto,x,y,controleur,"PowerSuperJump");  // herite de SuperPower
	this.player = player;
	this.compteur = 0;
	this.delta = 5;
	this.tmp = 0;

	  // load the spritesheet
 	this.spritesheet = libImg.getImage("rocket");
    this.spritesheet.width = 64;  // 640 = 64 * 10 (nb Image)   
  	this.spritesheet.height = 124; 

  	this.offset = 0;
  	this.currentFrame = 0;
  	this.then = performance.now();
  	this.totalTimeSinceLastRedraw = 0;
  	this.delayBetweenFrames = 1000 / 20;  // delay in ms between images

	this.apply = function(env,appel)
	{
		if(this.etat == appel) // si le mode d'appel est conforme à appel
								// un pouvoir automatique ne peut pas s'executer si il cherche à etre
								// éxécuté manuellement
		{
			var player = this.player;

			if(this.etat)
			{  // execution de type automatique

				if(this.tmp == this.delta)
				{

					if(this.compteur < 10) 
					{
						player.jump();
						this.compteur++;
						this.tmp = 0;
						this.consume = false;
						return false;
					}
					this.consume = true;
					return true;
				}
				this.tmp++;
			}

			else // execution de type -> manuelle
			{
				if(this.compteur < 10)
				{
					player.jump();
 
					this.compteur++;
					this.consume = false;
					return false;
				}
				this.consume = true;
				return true;
			}
		}
		this.consume = false;
		return false;
	};

	this.draw = function(env)
	{  
		if(this.consume) { return ;}	//si le pouvoir a été consommé (pas de draw)

		// Use time based animation to draw only a few images per second
    	var now = performance.now();
    	var delta = now - this.then; 
		
		//*\\//*\\//*\\ DRAW //*\\//*\\//*\\ 
		ctxCanvas = env.getContextCanvas();
		ctxCanvas.save();
		ctxCanvas.beginPath();
		
    	var scale = 4;
    	if(this.isDonne()) // si pouvoir donné
    	{
    		var player = this.player;
    		var xPlayer = player.posx;
    		var yPlayer = player.posy + env.posyA;
    		ctxCanvas.translate(xPlayer, yPlayer);
    		ctxCanvas.drawImage(this.spritesheet, this.offset, 0, this.spritesheet.width, this.spritesheet.height, -15, 0, 32, 32);
    	}
    	else
    	{
    		ctxCanvas.translate(this.x, this.y + env.posyA);
    		ctxCanvas.drawImage(this.spritesheet, 576, 0, this.spritesheet.width, this.spritesheet.height, -18, -10, 32,32);
    	}


  
    	ctxCanvas.fillStyle = this.color;
    	ctxCanvas.fill();  
    	ctxCanvas.closePath();
    	ctxCanvas.restore();
    	 //*\\//*\\//*\\ DRAW //*\\//*\\//*\\

	    if (this.totalTimeSinceLastRedraw > this.delayBetweenFrames)
	    {
	      // Go to the next sprite image
	      this.currentFrame++; 
	      this.currentFrame %= 10;  //this.spriteArray.length;  == nbImage
	      this.offset += this.spritesheet.width;
	      
	      if(this.currentFrame % 10 == 0) {
	         this.offset = 0;
	       }
	      //console.log("Offset " + this.offset);  // valeur max offset = 576
	          
	      // reset the total time since last image has been drawn
	      this.totalTimeSinceLastRedraw = 0;
	    } 

	    else {
	      // sum the total time since last redraw
	      this.totalTimeSinceLastRedraw += delta;
	    }

	    this.then = now;
	};


	this.abord = function(env)
	{
		this.consume = true;
		this.donne = false;
	}


}